module.exports = {
	content: ['./index.html', './src/*.{js,jsx,ts,tsx}', './src/**/*.{js,jsx,ts,tsx}'],
	theme: {
		extend: {},
	},
	plugins: [require('flowbite/plugin')],
	safeList: [],
	enabled: process.env.NODE_ENV === 'production',
};
